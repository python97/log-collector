#!/usr/bin/env python3.7
#
# File:   collector.py
# Author: Michal Swiatkowski
# E-mail: michal@stopzone.pl
#
# DB LOG COLLECTOR 2020
#
# v2.0
import sys
import logging
import socket
import datetime
import threading
import time
from os import path, rename
from subprocess import Popen
from daemons.prefab import run
from library.buffer import Buffer
from library.database import Db
from resource import getrusage, RUSAGE_SELF
from logging.handlers import TimedRotatingFileHandler


class GZipRotator:
    def __call__(self, source, dest):
        rename(source, dest)
        Popen(['gzip', dest])


class StartCollector(run.RunDaemon):

    db_path = None

    def run(self):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            sock.bind(('0.0.0.0', 514))
        except socket.error as err:
            logging.error("{}".format(err))
            sys.exit()

        logging.info('Server started')
        try:

            th_db = threading.Thread(target=Buffer.get_db_list, args=[self.db_path], daemon=True, name="db-list")
            th_db.start()

            th_analyse = threading.Thread(target=Buffer.start_data_analysis, daemon=True, name="analyser")
            th_analyse.start()

            th_stats = threading.Thread(target=StartCollector.stats, daemon=True, name="stats")
            th_stats.start()

            th_udp = threading.Thread(target=StartCollector.send_udp_packets, daemon=True, name="udp")
            th_udp.start()
        except Exception as er:
            logging.error(er)

        while True:
            message, ip = sock.recvfrom(1024)
            try:
                if len(message) is not None:
                    if ip[0] in Buffer.CONFIG:
                        Buffer.REQUESTS[ip[0]].append("{} {}".format(datetime.datetime.now(), message))
                        # logging.info('Received new data from {}'.format(ip[0]))
                    else:
                        logging.warning("{} - {}".format(ip, message))

            except Exception as error:
                logging.error("{}".format(error))

    @staticmethod
    def stats():
        logging.info('Thread: stats - started')
        while True:
            time.sleep(1800)
            logging.warning('##')
            logging.warning("## Buffer contains {} elements".format(len(Buffer.REQUESTS)))
            for i in Buffer.REQUESTS:
                logging.warning("##\t{}:\t{} elements".format(Buffer.CONFIG[i], len(Buffer.REQUESTS[i])))
            logging.warning('##')
            logging.warning('## Tables is memory:')
            for dbname in Db.TABLES:
                logging.warning("##\t{}:\t{} tables".format(dbname, len(Db.TABLES[dbname])))
            logging.warning('##')
            logging.warning('## Allowed DB names:')
            logging.warning("##\t{} databases".format(len(Buffer.DB_NAMES)))
            logging.warning('##')
            logging.warning('## Data waiting to write to DB:')
            for data in Buffer.DATA_TO_SAVE:
                logging.warning("##\t{}:\t{} tables".format(data, len(Buffer.DATA_TO_SAVE[data])))
            else:
                logging.warning("##\tnone")
            logging.warning('##')
            logging.warning('## Used memory: \t{} kB'.format(getrusage(RUSAGE_SELF).ru_maxrss))
            logging.warning('## Time elapsed:\t{}'.format((datetime.datetime.now() - Buffer.start_time)))
            logging.warning('##')

    @staticmethod
    def send_udp_packets():
        logging.info('Thread: udp - started')
        while True:
            for ip in Buffer.CONFIG:
                ss = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                server_address = ('{}'.format(ip), 10000)
                message = b'ARP table actualization from log collector.'
                try:
                    ss.sendto(message, server_address)
                except Exception as error:
                    logging.error("Can not send udp package to {}: {}".format(ip, error))
                finally:
                    ss.close()
            time.sleep(1)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        action = sys.argv[1]
        logfile = 'log/collector.log'
        pid_file = '/var/run/collector.pid'

        if len(sys.argv) > 2:
            if sys.argv[2] == "-d":
                # formatter = '%(asctime)s %(levelname)s %(message)s'
                # logging.basicConfig(filename=logfile, level=logging.DEBUG, format=formatter)

                formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
                handler = logging.handlers.TimedRotatingFileHandler(logfile, when="midnight", backupCount=15)
                handler.setFormatter(formatter)
                handler.rotator = GZipRotator()
                logger = logging.getLogger()
                logger.addHandler(handler)
                logger.setLevel(logging.DEBUG)

        Buffer.init_global_variables('config/collector.json')
        Buffer.start_time = datetime.datetime.now()

        run = StartCollector(pidfile=pid_file)
        run.db_path = path.dirname(path.abspath(__file__)) + '/config/db.list'

        if action == "start":
            run.start()

        elif action == "stop":
            run.stop()

        elif action == "restart":
            run.restart()

        elif action == "test":
            pass

    else:
        print('Usage: {} [start/stop/restart] [-d]'.format(sys.argv[0]))
        sys.exit()

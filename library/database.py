import logging
import datetime
import psycopg2 as psycopg2
from collections import defaultdict


class Db:
    TABLES = defaultdict(list)
    HOST = '172.16.110.152'
    USERNAME = 'postgres'
    PASSWORD = 'postgres'

    def __init__(self, db_name, table_name):
        self.table_name = table_name
        self.db_name = db_name

        try:
            self.db_connection = psycopg2.connect("dbname = '{}' user = '{}' host = '{}' password = '{}'".format(self.db_name, self.USERNAME, self.HOST, self.PASSWORD))
            self.db_cur = self.db_connection.cursor()
            logging.info("+ Connected to database: {} established".format(self.db_name))
        except (Exception, psycopg2.DatabaseError) as error:
            logging.critical("Error when connecting to database {}: {}".format(self.db_name, error))

        if len(self.TABLES) > 0 and len(self.TABLES[self.db_name]) > 0:
            if self.table_name not in self.TABLES[self.db_name]:
                self.check_table()
        else:
            self.check_table()

    def create_table(self):
        try:
            sql = "create table \"{}\" (" \
                  "id bigserial PRIMARY KEY, " \
                  "host VARCHAR(100), " \
                  "date VARCHAR(100), " \
                  "message text, " \
                  "state int default NULL" \
                  ");".format(self.table_name)

            self.db_cur.execute(sql)
            self.db_connection.commit()

            logging.warning("Created table {} in database {}".format(self.table_name, self.db_name))

        except (Exception, psycopg2.DatabaseError) as error:
            logging.critical("Create_table failed {} | {}".format(self.db_name, error))

    def check_table(self):
        try:
            sql = "SELECT EXISTS (" \
                  "SELECT 1 " \
                  "FROM information_schema.tables " \
                  "WHERE table_schema = 'public' " \
                  "AND table_name = '{}' " \
                  ");".format(self.table_name)

            self.db_cur.execute(sql)
            value, = self.db_cur.fetchone()

            if value is False:
                self.create_table()
                self.TABLES[self.db_name].append(self.table_name)
            else:
                self.TABLES[self.db_name].append(self.table_name)
                logging.info("+ Added table {} to database {}".format(self.table_name, self.db_name))

        except (Exception, psycopg2.DatabaseError) as error:
            logging.critical("Check_table failed {} | {}".format(self.db_name, error))

    def save_data_to_db(self, log_data, state=None):

        sql = "INSERT INTO \"{}\" (host, date, message, state) VALUES (%s, %s, %s, %s)".format(self.table_name)
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

        if isinstance(log_data, str):
            try:
                sql_data = (self.db_name, date, log_data, state)
                self.db_cur.execute(sql, sql_data)
                self.db_connection.commit()
                logging.info("### Save data in -- {} --> {} --".format(self.table_name, self.db_name))
            except Exception as exception:
                logging.critical("DB string: {} - {}".format(exception, log_data))

        elif isinstance(log_data, list):
            for log in log_data:
                try:
                    if 'ERROR' in log:
                        state = 1

                    sql_data = (self.db_name, date, log, state)
                    self.db_cur.execute(sql, sql_data)
                    self.db_connection.commit()

                    logging.info("### Save data in -- {} --> {} --".format(self.table_name, self.db_name))
                except Exception as exception:
                    logging.critical("DB list: {} - {}".format(exception, log_data))

        else:
            logging.critical('DB: wrong data type.')

    def __del__(self):
        self.db_connection.close()
        logging.info("- Connection to database {} has been closed".format(self.db_name))

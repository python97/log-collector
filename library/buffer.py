import datetime
import json
from collections import defaultdict
import logging
import time
from library.database import Db


class Buffer(object):
    # CONFIG -> IP:dbname
    CONFIG = {}
    DB_NAMES = []
    REQUESTS = defaultdict(list)
    DATA_TO_SAVE = defaultdict(list)
    start_time = None

    @staticmethod
    def init_global_variables(file):
        with open(file) as dataFile:
            data = json.loads(dataFile.read())
            for i in data["servers"]:
                Buffer.CONFIG[i["ip"]] = i["db"]

    @staticmethod
    def get_db_list(file_path):
        logging.info('Thread: db-list - started')
        while True:

            try:

                file = open(file_path, 'r')
                lines = file.readlines()
                for line in lines:
                    db = ''.join(line.splitlines())
                    if db not in Buffer.DB_NAMES and len(db) > 0 and db[0] != '#':
                        Buffer.DB_NAMES.append(db)
                file.close()

            except Exception as e:
                logging.error('{}'.format(e))

            time.sleep(600)

    @staticmethod
    def start_data_analysis():
        logging.info('Thread: analyse - started')
        while True:
            for ip in list(Buffer.REQUESTS):
                if isinstance(Buffer.REQUESTS[ip], (list,)) and len(Buffer.REQUESTS[ip]) > 0:
                    Buffer.get_data(ip)

                if len(Buffer.DATA_TO_SAVE) > 0:
                    Buffer.save_data(ip)
            time.sleep(0.1)

    @staticmethod
    def get_data(server_ip):

        disabled_chars = ['[', ']', '.', ',', '!', '?', '/', '\\', '@', '#', ':', ';']
        while True:

            if len(Buffer.REQUESTS[server_ip]) == 0:
                break

            time_now = time.mktime(datetime.datetime.now().timetuple())
            log = Buffer.REQUESTS[server_ip][0]
            log_time = " ".join(log.split()[0:2])

            try:
                time_last = time.mktime(datetime.datetime.strptime(log_time, '%Y-%m-%d %H:%M:%S.%f').timetuple())
            except ValueError:
                time_last = time.mktime(datetime.datetime.strptime(log_time, '%Y-%m-%d %H:%M:%S').timetuple())

            time_diff = int(time_now) - int(time_last)

            if time_diff > 1:

                if len(log.split()) > 11:

                    if log.split()[10] in ['CET', 'CEST'] and log.split()[11].islower():

                        if not any(disabled in log.split()[11] for disabled in disabled_chars):
                            table_name = log.split()[11]
                            start_number = log.split()[6]
                            process_id = log.split()[7]

                            if table_name not in Buffer.DATA_TO_SAVE:
                                Buffer.DATA_TO_SAVE[table_name] = []

                            tmp = [' '.join(log.split()[7:])]
                            del Buffer.REQUESTS[server_ip][0]

                            # not much logs in array, do sleep to wait for logs
                            if len(Buffer.REQUESTS[server_ip]) < 200:
                                time.sleep(0.5)

                            for log_check in Buffer.REQUESTS[server_ip][:]:

                                lname = log_check.split()
                                if len(lname) >= 6:
                                    if log_check.split()[6].split('-')[0] + '-' in start_number and '[{}]'.format(process_id) in log_check.split()[5]:
                                        tmp.append(' '.join(lname[7:]))
                                        Buffer.REQUESTS[server_ip].remove(log_check)
                                else:
                                    logging.error('Deleted log: {}'.format(log_check))
                                    Buffer.REQUESTS[server_ip].remove(log_check)

                            if table_name not in Buffer.DB_NAMES:
                                table_name = 'garbage'

                            Buffer.DATA_TO_SAVE[table_name].append("\n".join(tmp))
                            continue

                Buffer.save_data(server_ip, data=log, table='garbage')
                del Buffer.REQUESTS[server_ip][0]

            else:
                break

    @staticmethod
    def save_data(serverip, data=None, table=None, state=None):

        if isinstance(data, str):
            try:
                gdb = Db(Buffer.CONFIG[serverip], table)
                gdb.save_data_to_db(" ".join(data.split()[6:]), state)
                del gdb
            except Exception as error:
                logging.error("{} - {}".format(error, data))

        if data is None:
            for table_name in Buffer.DATA_TO_SAVE:
                if len(Buffer.DATA_TO_SAVE[table_name]) > 0:
                    try:
                        gdb = Db(Buffer.CONFIG[serverip], table_name)
                        gdb.save_data_to_db(Buffer.DATA_TO_SAVE[table_name])
                        del gdb
                    except Exception as error:
                        logging.error("{} - {}".format(error, data))

            Buffer.DATA_TO_SAVE.clear()

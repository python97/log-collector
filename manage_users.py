#!/usr/bin/env python3.7
#
# File:   manage_users.py
# Author: Michal Swiatkowski
# E-mail: michal#stopzone.pl
#
# MANAGE USERS 2019
#
# v1.1

import sys
import psycopg2
import string
import secrets
from collections import defaultdict


class Start:

    OPTIONS = {
        '0': "List all users",
        '1': "List all databases",
        '2': "List all tables",
        '3': "Change user password",
        '4': "List all tables in all databases for current user",
        '5': "Grant SELECT on tables for current user",
        '6': "Revoke SELECT privileges on tables for current user",
    }

    @staticmethod
    def show_options():
        print("Available options:\n")
        for i in Start.OPTIONS:
            Start.print_colored("\t{}".format(i), 'green', ' ')
            Start.print_colored("{}".format(Start.OPTIONS[i]), 'default')
        print()

    @staticmethod
    def print_colored(text, c="default", endl="\n"):
        color = {
            'blue': '\033[94m',
            'default': '\033[99m',
            'grey': '\033[90m',
            'yellow': '\033[93m',
            'black': '\033[90m',
            'cyan': '\033[96m',
            'green': '\033[92m',
            'magenta': '\033[95m',
            'white': '\033[97m',
            'red': '\033[91m'
        }
        if c in color:
            i = color[c]
            print("{}{}\033[0m".format(i, text), end=endl)
        else:
            print("{}".format(text))


class DBhandler:

    __HOST = '172.16.110.152'
    __USERNAME = 'postgres'
    __PASSWORD = 'postgres'

    db_connection = None
    db_cursor = None

    def __init__(self, dbname='postgres'):

        self.dbname = dbname

        try:
            # print("Open connection")
            self.db_connection = psycopg2.connect("dbname = '{}' user = '{}' host = '{}' password = '{}'".format(self.dbname, self.__USERNAME, self.__HOST, self.__PASSWORD))
            self.db_cursor = self.db_connection.cursor()

        except Exception as error:
            print("Error exception:\n{}".format(error))

    def __del__(self):
        self.db_connection.close()
        # print("Connection closed")


class ManageUsers:

    USERNAME = None
    PASSWORD = None

    def __init__(self, dbname, username):

        self.username = username
        self.dbname = dbname

        try:
            if self.check_user_exists():
                while True:
                    Start.print_colored("\n################################################", 'yellow')
                    Start.print_colored("\t### Current user:", 'yellow', ' ')
                    Start.print_colored("{}".format(self.USERNAME), 'blue', ' ')
                    Start.print_colored("###", 'yellow')
                    Start.print_colored("\t   ### Ctrl + C to exit ###", 'yellow')
                    Start.print_colored("################################################\n", 'yellow')
                    Start.show_options()
                    option = input('Select option [0-{}] > '.format(len(Start.OPTIONS) - 1))
                    if option in Start.OPTIONS:

                        dostuff = {
                            '0': self.list_users,
                            '1': self.list_db,
                            '2': self.list_all_tables,
                            '3': self.change_password,
                            '4': self.list_user_tables,
                            '5': self.grant_privileges,
                            '6': self.revoke_privileges
                        }

                        dostuff[option]()

                    else:
                        continue

        except KeyboardInterrupt:
            self.exit()
        except EOFError:
            self.exit()

    def exit(self):
        Start.print_colored("\n\n\tUsername: {}\n\tPassword: {}\n".format(self.USERNAME, self.PASSWORD), 'green')
        sys.exit()

    def check_user_exists(self):

        db = DBhandler()
        sql = "SELECT 1 FROM pg_roles WHERE rolname='{}'".format(self.username)
        db.db_cursor.execute(sql)
        value = db.db_cursor.fetchone()
        del db

        if value is None:
            Start.print_colored("User", 'default', '')
            Start.print_colored(" {} ".format(self.username), 'red', '')
            Start.print_colored("does not exist. Create new one? [T/N] > ".format(self.username), 'default', '')

            status = input()
            if status == 'T' or status == 't':
                if self.create_user():
                    return True
            else:
                return False
        else:
            ManageUsers.USERNAME = self.username
            return True

    def create_user(self):

        try:
            dbc = DBhandler()
            alphabet = string.ascii_letters + string.digits
            password = ''.join(secrets.choice(alphabet) for _ in range(35))

            sql = 'CREATE USER {} WITH PASSWORD \'{}\'; GRANT logreader TO {};'.format(self.username, password, self.username)
            dbc.db_cursor.execute(sql)
            dbc.db_connection.commit()
            del dbc

            ManageUsers.USERNAME = self.username
            ManageUsers.PASSWORD = password
            return True

        except BaseException as e:
            print(e)
            return False

    @staticmethod
    def list_users():

        db = DBhandler()
        sql = 'SELECT u.usename AS "username" ' \
              'FROM pg_catalog.pg_user u ' \
              'ORDER BY 1;'

        db.db_cursor.execute(sql)
        value = db.db_cursor.fetchall()

        print()
        Start.print_colored("\t{}".format(Start.OPTIONS['0']), 'green')
        print()

        for i in value:
            v, = i
            if v not in ['logreader', 'postgres']:
                print("\t{}".format(v))
        print()

    @staticmethod
    def list_db(ret=False):

        db = DBhandler()

        sql = 'SELECT datname FROM pg_database ' \
              'WHERE datistemplate = false ' \
              'ORDER BY 1;'

        db.db_cursor.execute(sql)
        value = db.db_cursor.fetchall()

        if not ret:
            print()
            Start.print_colored("\t{}".format(Start.OPTIONS['1']), 'green')
            print()

        out = []

        for i in value:
            v, = i
            if v not in 'postgres':
                if ret:
                    out.append(v)
                else:
                    print("\t{}".format(v))

        if len(out) > 0:
            return out
        else:
            print()

    def list_all_tables(self):

        print()
        Start.print_colored("\t{}".format(Start.OPTIONS['2']), 'green')
        print()

        db = self.list_db(True)
        for i in db:
            Start.print_colored("\tDB: {}".format(i), 'red')
            tables = self.list_tables(i)
            for t in tables:
                print("\t\t- {}".format(t))
            print()

    @staticmethod
    def list_tables(database):

        out = []

        if database not in 'postgres':

            tsql = "SELECT table_name " \
                   "FROM information_schema.tables " \
                   "WHERE table_schema = 'public' " \
                   "ORDER BY 1;"

            tdb = DBhandler(database)
            tdb.db_cursor.execute(tsql)
            tables = tdb.db_cursor.fetchall()

            for t in tables:
                o, = t
                out.append(o)

            return out

    def change_password(self):

        db = DBhandler()

        alphabet = string.ascii_letters + string.digits
        password = ''.join(secrets.choice(alphabet) for _ in range(35))

        sql = "ALTER USER {} WITH PASSWORD '{}'".format(self.USERNAME, password)

        db.db_cursor.execute(sql)
        db.db_connection.commit()
        del db

        self.PASSWORD = password
        Start.print_colored("\n\tPassword changed to: {}\n".format(password), 'green')

    def list_user_tables(self):

        value = self.list_db(True)

        print()
        Start.print_colored("\t{}".format(Start.OPTIONS['4']), 'green')
        print()

        for v in value:
            if v not in 'postgres':
                Start.print_colored("\tDB: {}".format(v), 'red')

                tsql = "SELECT table_name " \
                       "FROM information_schema.table_privileges " \
                       "WHERE  grantee = '{}' " \
                       "AND table_schema = 'public' " \
                       "ORDER BY 1".format(self.USERNAME)

                tdb = DBhandler(v)
                tdb.db_cursor.execute(tsql)
                tables = tdb.db_cursor.fetchall()
                del tdb

                for t in tables:
                    o, = t
                    print("\t\t- {}".format(o))

                print()

    def start_privileges(self):

        dblist = self.list_db(True)
        print("\n\t{}\n".format(dblist))
        dbselect = input("Select database > ")

        out = defaultdict(list)

        if dbselect in dblist:
            tab = self.list_tables(dbselect)
            print()
            for o in tab:
                print("\t\t- {}".format(o))
            print()
            tb = input('Select tables [split by space] > ')

            if len(tb) > 0:
                for i in tb.split():
                    if i in tab:
                        out[dbselect].append(i)

        else:
            Start.print_colored("\nDB does not exist.", 'red')

        return out

    def grant_privileges(self):

        print()
        Start.print_colored("\t{}".format(Start.OPTIONS['5']), 'green')

        start = self.start_privileges()
        if len(start) > 0:

            for dbname in start:
                db = DBhandler(dbname)

                for i in start[dbname]:
                    sql = 'GRANT SELECT ON {} TO {};'.format(i, self.USERNAME)
                    db.db_cursor.execute(sql)

                db.db_connection.commit()
                del db

            Start.print_colored("\nPrivileges was actualized.", 'green')

        else:
            Start.print_colored("\nNo tables was selected.", 'red')

    def revoke_privileges(self):

        print()
        Start.print_colored("\t{}".format(Start.OPTIONS['6']), 'green')

        start = self.start_privileges()
        if len(start) > 0:

            for dbname in start:
                db = DBhandler(dbname)

                for i in start[dbname]:
                    sql = 'REVOKE SELECT ON {} FROM {};'.format(i, self.USERNAME)
                    db.db_cursor.execute(sql)

                db.db_connection.commit()
                del db

            Start.print_colored("\nPrivileges was actualized.", 'green')

        else:
            Start.print_colored("\nNo tables was selected.", 'red')


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print("\nWrite username as parameter.\n")
        sys.exit()

    par = sys.argv[1]
    if par.isalpha() and par.islower():
        ManageUsers('postgres', sys.argv[1])
    else:
        print("\nThe username must contain only lowercase letters.\n")
